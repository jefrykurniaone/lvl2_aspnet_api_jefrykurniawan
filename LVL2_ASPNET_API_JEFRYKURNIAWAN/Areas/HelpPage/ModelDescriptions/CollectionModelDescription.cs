namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.Areas.HelpPage.ModelDescriptions
{
    public class CollectionModelDescription : ModelDescription
    {
        public ModelDescription ElementDescription { get; set; }
    }
}