using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.Areas.HelpPage.ModelDescriptions
{
    public class EnumTypeModelDescription : ModelDescription
    {
        public EnumTypeModelDescription()
        {
            Values = new Collection<EnumValueDescription>();
        }

        public Collection<EnumValueDescription> Values { get; private set; }
    }
}