using System;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.Areas.HelpPage.ModelDescriptions
{
    public class ParameterAnnotation
    {
        public Attribute AnnotationAttribute { get; set; }

        public string Documentation { get; set; }
    }
}