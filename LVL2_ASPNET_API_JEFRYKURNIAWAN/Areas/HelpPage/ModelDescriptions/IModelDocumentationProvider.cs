using System;
using System.Reflection;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}