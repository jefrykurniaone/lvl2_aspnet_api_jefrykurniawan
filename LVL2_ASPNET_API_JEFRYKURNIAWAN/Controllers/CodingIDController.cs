﻿using LVL2_ASPNET_API_JEFRYKURNIAWAN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.Controllers
{
    public class CodingIDController : ApiController
    {
        DB_CodingIDEntities db = new DB_CodingIDEntities();
        
        // GET api/<controller>
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            List<ms_employee> list_employee = new List<ms_employee>();
            //list_employee = db.ms_employee.ToList();
            int limit_page = 2;
            list_employee = db.ms_employee.OrderBy(x => x.pk_ms_employee_id).Skip((id-1)*limit_page).Take(limit_page).ToList();
            return Ok(list_employee);
        }

        // GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/<controller>
        public IHttpActionResult Post([FromBody]ms_employee param)
        {
            Console.WriteLine(param);

            db.ms_employee.Add(param);
            db.SaveChanges();
            return Ok();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}