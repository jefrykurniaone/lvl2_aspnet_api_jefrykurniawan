﻿using LVL2_ASPNET_API_JEFRYKURNIAWAN.Models;
using Microsoft.Owin.Security.OAuth;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN.App_Start
{
    public class MyAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            //Declare obj entities
            DB_CodingIDEntities db = new DB_CodingIDEntities();

            ms_user userdata = db.ms_user.Where(x => x.username == context.UserName && x.password == context.Password).FirstOrDefault();

            if (userdata != null)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, userdata.role));
                identity.AddClaim(new Claim(ClaimTypes.Name, userdata.username));
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                context.Rejected();
            }
        }
    }
}