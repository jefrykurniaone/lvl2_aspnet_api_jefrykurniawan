﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNET_API_JEFRYKURNIAWAN
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
